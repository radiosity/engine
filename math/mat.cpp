// Copyright (c) 2015 Contributors as noted in the AUTHORS file.
// This file is part of form_factors.
//
// thorium is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// thorium is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with thorium.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This module contains math operations between matrices and vectors.
 */

#include "mat.h"
#include <cstdio>

namespace math
{
  mat33 rotate_towards(vec3 subject, vec3 to)
  {
    vec3 v = cross(subject, to);
    point_t s2 = dot(v, v);
    point_t c = dot(subject, to); // TODO: Normalize?

    mat33 rot = IDENTITY_33();

    if (s2 > FLT_MIN)
    {
      mat33 ssc = make_mat33(0, -v.z, v.y, v.z, 0, -v.x, -v.y, v.x, 0);
      mat33 ssc2 = ssc_mul(ssc, ssc);
      ssc2 = ssc2 * ((1 - c) / s2);
      rot = rot + ssc + ssc2;
    }
    else
    {
      vec3 f = subject + to;
      float l = dot(f, f);
      if (l < FLT_EPSILON)
        rot = REVERSE_33();
    }

    return rot;
  }

  math::mat33 axis_rotation(float x, float y, float z)
  {
    math::mat33 rx = math::make_mat33(1, 0, 0, 0, cosf(x), -sinf(x), 0, sinf(x), cosf(x));
    math::mat33 ry = math::make_mat33(cosf(y), 0, sinf(y), 0, 1, 0, -sinf(y), 0, cosf(y));
    math::mat33 rz = math::make_mat33(cosf(z), -sinf(z), 0, sinf(z), cosf(z), 0, 0, 0, 1);
    return rx * ry * rz;
  }

  bool load_from_string(const char* str, mat44& m)
  {
    return sscanf(str, "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f",
      m.p + 0, m.p + 1, m.p + 2, m.p + 3,
      m.p + 4, m.p + 5, m.p + 6, m.p + 7,
      m.p + 8, m.p + 9, m.p + 10, m.p + 11,
      m.p + 12, m.p + 13, m.p + 14, m.p + 15) == 16;
  }

  vec3 translation(const mat44& m)
  {
    return make_vec3(m._14, m._24, m._34);
  }

  mat33 rotation(const mat44& m)
  {
    return make_mat33(m._11, m._12, m._13,
      m._21, m._22, m._23,
      m._31, m._32, m._33
      );
  }
}
