import argparse
import logging
from collections import defaultdict
from vector_math import triangle_area, tetrahedron_volume


def normalize_exp_sgn(s, sgn):
    pcs = s.split(sgn)
    if len(pcs) == 1:
        return s
    if pcs[0][-1] != 'e' or pcs[0][-1] != 'E':
        pcs[0] += 'e'
    return sgn.join(pcs)


def normalize_exp(s):
    if '+' in s:
        return normalize_exp_sgn(s, '+')
    elif '-' in s:
        return normalize_exp_sgn(s, '-')
    else:
        return s


def normalize(s):
    pcs = s.split('.')
    if len(pcs) == 1:
        return s
    pcs[-1] = normalize_exp(pcs[-1])
    return '.'.join(pcs)

    
def parse_float(s):
    s = s.strip()
    try:
        return float(s) if s else 0.0
    except ValueError:
        s = normalize(s)
        return float(s)


def parse_int(s):
    s = s.strip()
    return int(s) if s else 0


class Top(object):
    def __init__(self, diffuse=0, specular=0, absorbance=0, transmittance=0, emissivity=0):
        self.diffuse = diffuse
        self.specular = specular
        self.absorbance = absorbance
        self.transmittance = transmittance
        self.emissivity = emissivity


class Material(object):
    def __init__(self, name=None, front=None, rear=None, density=None, thickness=None, heat_capacity=None, thermal_conductivity=None):
        self.name = name
        self.front = front
        self.rear = rear
        self.density = density
        self.heat_capacity = heat_capacity
        self.thickness = thickness
        self.thermal_conductivity = thermal_conductivity

        
class Bdf2Obj(object):
    def __init__(self):
        self.meshes_elements = defaultdict(list)
        self.vertices = dict()
        self.meshes_names = dict()
        self.meshes_materials = dict()
        self.mesh_thickness = dict()
        self.tops = dict()
        self.materials = defaultdict(Material)
        
    def write_parsed(self, obj_filename, mtl_filename):
        self._flatten()
        with open(obj_filename, 'wb') as f:
            f.write(b'mtllib %s\n' % mtl_filename.encode())
            self._write_vertices(f)
            self._write_meshes(f)
        with open(mtl_filename, 'wb') as f:
            self._write_materials(f)            
                
    def _write_vertices(self, f):
        vertices = sorted(self.vertices.items(), key=lambda x: x[0])
        for _idx, v in vertices:
            f.write(b'v %.14f %.14f %.14f\n' % v)
        f.write(b'\n')
    
    def _write_materials(self, f):
        for mesh, material_index in self.meshes_materials.items():        
            name = 'mtl_%s' % self._get_mesh_name(mesh)
            thickness = self.mesh_thickness.get(mesh, float("nan"))
            
            material = self.materials.get(material_index)
            material.name = name
            material.thickness = thickness
            
            self._write_material(f, material)            
    
    def _write_material(self, f, material):
        f.write(b'newmtl %s\n' % material.name.encode())
        f.write(b'shell.density %f\n' % material.density)
        f.write(b'shell.heat_capacity %f\n' % material.heat_capacity)
        f.write(b'shell.thermal_conductivity %f\n' % material.thermal_conductivity)
        f.write(b'shell.thickness %f\n' % material.thickness)
        self._write_top(f, b'front', material.front)
        self._write_top(f, b'rear', material.rear)
        f.write(b'\n')
            
    def _write_top(self, f, side, top):
        f.write(b'%s.specular_reflectance %f\n' % (side, top.specular))
        f.write(b'%s.diffuse_reflectance %f\n' % (side, top.diffuse))
        f.write(b'%s.absorbance %f\n' % (side, top.absorbance))
        f.write(b'%s.transmittance %f\n' % (side, top.transmittance))
        f.write(b'%s.emissivity %f\n' % (side, top.emissivity))
        
    def _get_mesh_name(self, index):
        return self.meshes_names.get(index, 'mesh_%d' % index)
    
    def _write_meshes(self, f):
        for mesh, elements in self.meshes_elements.items():
            material = self.meshes_materials.get(mesh)
            mesh_name = self._get_mesh_name(mesh).encode()
            material_name = b'mtl_%s' % mesh_name

            for element_index, element in elements:
                f.write(b'g %s_%d\n' % (mesh_name, element_index))
                if material:
                    f.write(b'usemtl %s\n' % material_name)
                for face in element:
                    f.write(b'f %d %d %d\n' % face)
                f.write(b'\n')
                
    def _flatten(self):
        remap = dict()
        for index, _ in self.vertices.items():
            new_index = 1 + len(remap)
            remap[index] = new_index
        new_meshes = defaultdict(list)
        for mesh, elements in self.meshes_elements.items():
            new_elements = list()
            for element_index, element in elements:
                new_element = list()
                for v0, v1, v2 in element:
                    new_face = (remap[v0], remap[v1], remap[v2])
                    new_element.append(new_face)
                new_element = list(sorted(new_element))
                new_elements.append((element_index, new_element))
            new_meshes[mesh] = new_elements
        self.meshes_elements = new_meshes

    def validate(self):
        failed = False
        for mesh, elements in self.meshes_elements.items():
            for element_index, faces in elements:
                points = set(v for face in faces for v in face)
                is_tetrahedron = len(points) == 4
                if is_tetrahedron:
                    points = [self.vertices[p] for p in points]
                    volume = tetrahedron_volume(*points)
                    if volume < 1e-9:
                        logging.error('Mesh %d tetrahedron element %d has degenerative volume %e' % (mesh, element_index, volume))
                for face in faces:
                    vertices = []
                    for v in face:
                        vertex = self.vertices.get(v)
                        vertices.append(vertex)
                        if vertex is None:
                            logging.error('Missing mesh %d element %d vertex %d' % (mesh, element_index, v))
                            failed = True
                    area = triangle_area(*vertices)
                    if area < 1e-6:
                        logging.error('Mesh %d element %d face has degenerative area %e' % (mesh, element_index, area))
                    if failed:
                        raise RuntimeError('Model validation failed')

    def parse_top(self, filename):
        with open(filename) as f:
            lines = [x for x in f.read().split('\n')]
            self._parse_top_lines(lines)
    
    def _parse_top_lines(self, lines):
        medium_line = None
        for line in lines:
            if medium_line:
                self.parse_medium1(medium_line, line)
                medium_line = None
            elif line.startswith('FTOP'):
                self.parse_ftop(line)
            elif line.startswith('MEDIUM1'):
                medium_line = line
                
    def parse_medium1(self, l0, l1):
        index = parse_int(l0[8:16])
        thermal_conductivity = parse_float(l0[32:40])
        heat_capacity = parse_float(l0[40:48])
        front = self.tops.get(parse_int(l1[8:16]))
        rear = self.tops.get(parse_int(l1[16:24]))
        
        material = self.materials[index]
        material.front = front
        material.rear = rear
        material.thermal_conductivity = thermal_conductivity
        material.heat_capacity = heat_capacity
                
    def parse_ftop(self, line):
        index = parse_int(line[8:16])
        diffuse = parse_float(line[16:24])
        specular = parse_float(line[24:32])
        absorbance = parse_float(line[32:40])
        transmittance = parse_float(line[40:48])        
        emissivity = parse_float(line[48:56])
        
        top = Top(
            diffuse=diffuse,
            specular=specular,
            absorbance=absorbance,
            transmittance=transmittance,
            emissivity=emissivity,
        )
        self.tops[index] = top

    def parse_bdf(self, filename):
        with open(filename) as f:
            lines = [x for x in f.read().split('\n')]
            self._parse_bdf_lines(lines)
    
    def _parse_bdf_lines(self, lines):
        grid_line = None
        active_mesh = None
        active_material_name = None
        for line in lines:
            if active_mesh:
                if line.startswith('$ Pset: '):
                    name = line.split(' ')[2][1:-1]
                    print('Set mesh %d name %s' % (active_mesh, name))
                    self.meshes_names[active_mesh] = name
                active_mesh = None
            elif line.startswith('$ Material Record'):
                active_material_name = line.split(' ')[4]
            elif grid_line:
                self.parse_grid_start(grid_line, line)
                grid_line = None
            elif line.startswith('CQUAD4'):
                self.parse_cquad4(line)
            elif line.startswith('CTRIA3'):
                self.parse_ctria3(line)
            elif line.startswith('CTETRA'):
                self.parse_ctetra(line)
            elif line.startswith('GRID*'):
                grid_line = line
                continue
            elif line.startswith('GRID'):
                self.parse_grid(line)
            elif line.startswith('PSHELL'):
                active_mesh = self.parse_pshell(line)
            elif line.startswith('PSOLID'):
                active_mesh = self.parse_psolid(line)
            elif line.startswith('MAT1'):
                index = self.parse_mat1(line, active_material_name)
                active_material_name = None
                
    def parse_mat1(self, line, name):
        index = parse_int(line[9:16])
        density = parse_float(line[40:48])
        material = self.materials[index]
        if name:
            print('Set material %d name %s' % (index, name))
        material.name = name
        material.density = density
    
    def parse_pshell(self, line):
        index = parse_int(line[9:17])
        material = parse_int(line[17:24])
        thickness = parse_float(line[24:32])
        self.mesh_thickness[index] = thickness
        self.meshes_materials[index] = material
        return index

    def parse_psolid(self, line):
        index = parse_int(line[9:17])
        material = parse_int(line[17:24])
        self.mesh_thickness[index] = 0
        self.meshes_materials[index] = material
        return index

    def parse_grid(self, line):
        index = parse_int(line[9:17])
        x = parse_float(line[24:32])
        y = parse_float(line[32:40])
        z = parse_float(line[40:48])
        self.vertices[index] = (x, y, z)

    def parse_grid_star(self, l0, l1):
        index = parse_int(l0[9:17])
        x = parse_float(l0[40:56])
        y = parse_float(l0[56:72])
        z = parse_float(l1[7:24])
        self.vertices[index] = (x, y, z)

    def parse_ctetra(self, line):
        index = parse_int(line[9:17])
        mesh = parse_int(line[17:25])
        v = [
            parse_int(line[25:33]),
            parse_int(line[33:41]),
            parse_int(line[41:49]),
            parse_int(line[49:57])
        ]

        faces = [
            (v[0], v[1], v[2]),
            (v[0], v[2], v[3]),
            (v[0], v[3], v[1]),
            (v[1], v[3], v[2]),
        ]
        self._add_mesh_element(mesh, index, faces)

    def parse_cquad4(self, line):
        index = parse_int(line[9:17])
        mesh = parse_int(line[17:25])
        v = [
            parse_int(line[25:33]),
            parse_int(line[33:41]),
            parse_int(line[41:49]),
            parse_int(line[49:57])
        ]

        faces = [
            (v[0], v[1], v[2]),
            (v[0], v[2], v[3]),
        ]
        self._add_mesh_element(mesh, index, faces)

    def parse_ctria3(self, line):
        index = parse_int(line[9:17])
        mesh = parse_int(line[17:25])
        v = [
            parse_int(line[25:33]),
            parse_int(line[33:41]),
            parse_int(line[41:49]),
            ]
        
        faces = [(v[0], v[1], v[2])]
        self._add_mesh_element(mesh, index, faces)
        
    def _add_mesh_element(self, mesh, element_index, element):
        logging.debug('Add mesh element', mesh, element)
        self.meshes_elements[mesh].append((element_index, element))


def main():
    parser = argparse.ArgumentParser(description='DBF to Thorium obj converter')
    parser.add_argument('bdf', type=str, help='Input Patran bdf filename')
    parser.add_argument('top', type=str, help='Thorium thermo-optical properties filename')
    parser.add_argument('obj', type=str, help='Output scene .obj filename')
    parser.add_argument('mtl', type=str, help='Output materials databse .mtl filename')
    args = parser.parse_args()

    parser = Bdf2Obj()
    parser.parse_bdf(args.bdf)
    parser.parse_top(args.top)
    parser.validate()
    parser.write_parsed(args.obj, args.mtl)


if __name__ == "__main__":
    main()

