// Copyright (c) 2015 Contributors as noted in the AUTHORS file.
// This file is part of form_factors.
//
// thorium is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// thorium is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with thorium.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This module contains basic types to represent a scene for ray caster calculation.
 * Module also contains base type (system_t) for ray caster with table of virtual methods.
 */

#pragma once

#include "../math/types.h"
#include "../subject/system.h"

namespace ray_caster
{
  typedef math::triangle_t face_t;
  typedef math::ray_t ray_t;

  struct rays_data_t
  {
    int n_tasks;
    math::ray_t* ray;
    int* hit_face_idx;
    float* hit_distance; /// Distance from ray origin to hit point.
    float* power;
  };

  /**
   * @brief Ray caster task representation.
   * @detail Each task consists of n_tasks rays (input), indices of hit scene faces and hit points (output variables).
   * If ray has no intersection with scene face it index will be -1.
   */
  struct task_t : rays_data_t
  { 
    int allocation_device;

    // @todo Move methods to separate table
    void(*free_data)(task_t*);
    int(*grow)(task_t*, int addition);    
    int(*relocate)(task_t*, int device); /// @detail Looks useless but it is not. It allows to preserve knowledge about specific allocation.
    
    int n_reserved;
    int n_missed;

    rays_data_t next_gen;
  };

  /// @brief Allocates memory for task with n_rays rays.
  task_t* task_create(int n_rays);
  
  /// @brief Grow existing or create new task.
  int task_grow(task_t** task, int n_rays);

  /// @return current reserve.
  int task_reserve(task_t** task, int n_rays);
  int task_get_reserve(task_t* task);
  int task_get_n_tasks(task_t* task);

  void task_next_gen(task_t* task);

  void task_free_data(task_t* task);

  /// @brief Frees memory for task.
  void task_free(task_t* task);

  /**
   * @brief Ray caster type.
   *
   * Represents base type for ray caster implementations with virtual methods table.
   */
  struct system_t
  {
    /// @brief virtual methods table.
    const struct system_methods_t* methods;
  };

  #define RAY_CASTER_OK    0
  #define RAY_CASTER_ERROR 1
  #define RAY_CASTER_NOT_SUPPORTED 2
  #define RAY_CASTER_INVALID_TASK_ALLOCATION 3

  /**
   *   @brief Virtual methods table for ray caster functionality.
   *
   *   Every concrete calculator should implement these methods.
   *   C-interface functions (system_* functions, see below) just allocate memory and call these virtual methods.
   */
  struct system_methods_t
  {
    // @todo Add double init/shutdown check in base ray caster system.

    /// @brief Initializes system after creation.
    int(*init)(system_t* system);

    /// @brief Shutdowns system prior to free memory.
    int(*shutdown)(system_t* system);

    /// @brief Sets loaded scene (polygons in meshes) for ray caster.
    int(*set_scene)(system_t* system, subject::scene_t* scene);

    /// @brief Checks system consistency and prepare scene for ray casting.
    int(*update)(system_t* system);

      /// @brief Casts rays of given task task for prepared scene.
    /// @note Task's rays, rays hit faces indices and points are prepared by callee.
    int(*cast)(system_t* system, task_t* task);

    /// @brief Relocate task to device.
    /// @detail device -1 reserved for host, 0 for ray caster native device.
    int (*relocate_task)(system_t* system, task_t* task, int device);
  };

  const char* system_get_name(int system_id);
  const char* system_get_id(int system_id);

  #define RAY_CASTER_NAIVE_CPU 0
  #define RAY_CASTER_NAIVE_CUDA 1
  #define RAY_CASTER_OPTIX 4
  #define RAY_CASTER_OPTIX_CPU 5
  #define RAY_CASTER_SYSTEMS_COUNT 6

  /**
   * @brief Factory method for ray caster creation.
   * @param type[in] @see RAY_CASTER_NAIVE_CPU for CPU caster and @see RAY_CASTER_NAIVE_CUDA for GPU.
   * @note init() system on creation.
   */
  system_t* system_create(int type);

  /// @brief Creates default ray caster system (CPU).
  system_t* system_create_default();
  
  /// @brief Frees ray caster resources.
  /// @note shutdown() system on destruction.
  void system_free(system_t* system);

  /// Here go C-interface wrappers to call system_t's virtual methods.

  int system_init(system_t* system);
  int system_shutdown(system_t* system);
  int system_set_scene(system_t* system, subject::scene_t* scene);
  int system_update(system_t* system);
  int system_cast(system_t* system, task_t* task);
  int system_relocate_task(system_t* system, task_t* task, int device);

  math::vec3 task_hit_point(const task_t* task, int n_ray);
}
