// Copyright 2015 Stepan Tezyunichev (stepan.tezyunichev@gmail.com).
// This file is part of form_factors.
//
// thorium is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// thorium is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with thorium.  If not, see <http://www.gnu.org/licenses/>.

#include "gtest/gtest.h"

#include "import_export/obj_import.h"

using namespace testing;

TEST(ObjImport, ParseFaces)
{
  subject::scene_t* scene = 0;
  ASSERT_EQ(OBJ_IMPORT_OK, obj_import::scene("models/parallel_planes.obj", &scene));
  ASSERT_EQ(4, scene->n_faces);
}

TEST(ObjImport, ParseMeshes)
{
  subject::scene_t* scene = 0;
  ASSERT_EQ(OBJ_IMPORT_OK, obj_import::scene("models/parallel_planes.obj", &scene));
  ASSERT_EQ(2, scene->n_meshes);
}

TEST(ObjImport, DetectUnknownMaterial)
{
  subject::scene_t* scene = 0;
  ASSERT_EQ(-OBJ_IMPORT_MATERIAL_NOT_DEFINED, obj_import::scene("models/unknown_material.obj", &scene));
}

TEST(ObjImport, ParseMaterials)
{
  subject::scene_t* scene = 0;
  ASSERT_EQ(OBJ_IMPORT_OK, obj_import::scene("models/test_material.obj", &scene));
  ASSERT_EQ(1, scene->n_materials);
  subject::material_t& m = scene->materials[0];
  ASSERT_NEAR(1, m.shell.density, 0.01);
  ASSERT_NEAR(2, m.shell.heat_capacity, 0.01);
  ASSERT_NEAR(3, m.shell.thermal_conductivity, 0.01);
  ASSERT_NEAR(4, m.shell.thickness, 0.01);
  ASSERT_NEAR(1.1, m.front.optical.specular_reflectance, 0.01);
  ASSERT_NEAR(1.2, m.front.optical.diffuse_reflectance, 0.01);
  ASSERT_NEAR(1.3, m.front.optical.absorbance, 0.01);
  ASSERT_NEAR(1.4, m.front.optical.transmittance, 0.01);
  ASSERT_NEAR(1.5, m.front.emission.emissivity, 0.01);
  ASSERT_NEAR(2.1, m.rear.optical.specular_reflectance, 0.01);
  ASSERT_NEAR(2.2, m.rear.optical.diffuse_reflectance, 0.01);
  ASSERT_NEAR(2.3, m.rear.optical.absorbance, 0.01);
  ASSERT_NEAR(2.4, m.rear.optical.transmittance, 0.01);
  ASSERT_NEAR(2.5, m.rear.emission.emissivity, 0.01);
}

TEST(ObjImport, Orbit)
{
  ballistics::tle_orbit_t* params = 0;
  ASSERT_EQ(OBJ_IMPORT_OK, obj_import::orbit("models/zarya.orbit", &params));
  ASSERT_STREQ("ISS (ZARYA)", params->tle.header);
  ASSERT_STREQ("1 25544U 98067A   16281.78259727  .00005772  00000-0  94529-4 0  9999", params->tle.line_1);
  ASSERT_STREQ("2 25544  51.6441 216.4056 0006619  52.6341  35.2822 15.54077957 22501", params->tle.line_2);
  ASSERT_EQ(ATTITUDE_TYPE_ORBIT, params->attitude.type);
  tle_orbit_free(params);
}
